package com.shhc.crawler;

import java.io.File;

/**  
* 清理一些空的文件夹   
*  
* @author  Gaogl  
* @date 2018年4月4日  新建  
*/
public class ClearEmptyDirs
{
    static int i = 0;

    public static void main( String[] args )
    {
        // 文件夹清理的开始位置，默认为d:\pictures
        String dir_str = "d:\\pictures";
        File dir = new File( dir_str );
        clear( dir );
        System.out.println( "清理完毕。" );
        System.out.println( "共删除了" + i + "个空文件夹" );
    }

    public static void clear( File dir )
    {
        File[] dir2 = dir.listFiles();
        for( int i = 0; i < dir2.length; i++ )
        {
            if( dir2[i].isDirectory() )
            {
                clear( dir2[i] );
            }
        }
        if( dir.isDirectory() && dir.delete() )
            i++;
        System.out.println( dir + "删除成功" );

    }

}
