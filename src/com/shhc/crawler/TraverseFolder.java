package com.shhc.crawler;

import java.io.File;

/**  
* 根据指定的目录位置判断文件夹是否为空,当多线程爬取大量关键字
* 图片时,可能会出现空文件夹的情况,可以执行该方法得到空文件夹   
*  
* @author  Gaogl  
* @date 2018年4月2日  新建  
*/
public class TraverseFolder {
	public static void main(String[] args) {
		traverseFolder2("G:\\菜品图片");
	}
	public static void traverseFolder2(String path) {

        File file = new File(path);
        if (file.exists()) {
            File[] files = file.listFiles();
            if (files.length == 0) {
                System.out.println("文件夹是空的!");
                return;
            } else {
                for (File file2 : files) {
                    if (file2.isDirectory()) {
                        System.out.println("文件夹:" + file2.getAbsolutePath());
                        traverseFolder2(file2.getAbsolutePath());
                    } else {
                        System.out.println("文件:" + file2.getAbsolutePath());
                    }
                }
            }
        } else {
            System.out.println("文件不存在!");
        }
    }
}
